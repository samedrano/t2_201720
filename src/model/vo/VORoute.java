package model.vo;

import model.logic.Rutas;

/**
 * Representation of a route object
 */
public class VORoute implements Comparable<VORoute>
{
	private int id;
	private String name;
	
	public VORoute(int pId, String pName)
	{
		id= pId;
		name = pName;
	}

	/**
	 * @return id - Route's id number
	 */
	public int id() {
		return id;
		// TODO Auto-generated method stub
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public int compareTo(VORoute r) {
		// TODO Auto-generated method stub
		return id - r.id();
	}

}
