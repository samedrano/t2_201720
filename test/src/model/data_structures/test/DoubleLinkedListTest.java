package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;

public class DoubleLinkedListTest<T> extends TestCase 
{
	
	private DoubleLinkedList<String> listaDoble;
	
	//--------------------------------------------
	//Escenarios
	//-------------------------------------------- 
	
	public void setUpEscenario1()
	{
		listaDoble = new DoubleLinkedList<String>();
	}
	
	public void setUpEscenario2()
	{
		listaDoble = new DoubleLinkedList<String>();			
		listaDoble.addAtEnd("Juan");
		listaDoble.addAtEnd("Mario");
		listaDoble.addAtEnd("el");
	}
	
	//--------------------------------------------
	//Metodos
	//-------------------------------------------- 
	
	public void testGetSize()
	{
		setUpEscenario1();
		assertEquals("La lista deberia de estar vacia", 0, listaDoble.getSize());
		
		setUpEscenario2();
		assertEquals("La lista deberia tener 3 elementos", 3, listaDoble.getSize());	
	}
	public void testBorrarNodo()
	{
		setUpEscenario2();
		
		boolean n = listaDoble.borrarNodo("Juan");
		int t = listaDoble.getSize();
		String p = listaDoble.darPrimero().darInformacion();
		
		assertEquals("no se elimin� el elemento", true, n);
		assertEquals("no se elimin� el elemento", 2, t);
		assertEquals("no se elimin� el elemento", "Mario", p);		
	}	
	public void testAddAtEnd()
	{
		setUpEscenario1();
		
		listaDoble.addAtEnd("nuevo");
		int n=listaDoble.getSize();
		assertEquals("no se agreg� el elemento", 1, n);
		
		setUpEscenario2();
		listaDoble.addAtEnd("nuevo");
		String h = listaDoble.darUltimo().darInformacion();
		assertEquals("no se agreg� el elemento", "nuevo", h);
	}
	public void testAgregarDespuesDe()
	{
        setUpEscenario2();
        
        listaDoble.agregarDespuesDe("noExiste", "nuevo");	
		boolean agrego = listaDoble.agregarDespuesDe("noExiste", "nuevo");
		assertEquals("no se agreg� el elemento", false, agrego);
		
		listaDoble.agregarDespuesDe("Juan", "nuevo");
		int n=listaDoble.getSize();
		assertEquals("no se agreg� el elemento", 4, n);
		
		listaDoble.agregarDespuesDe("el", "nuevo");
		int k =listaDoble.getSize();
		assertEquals("no se agreg� el elemento", 5, k);
	}
	
}
