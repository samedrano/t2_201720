package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.ListaCircular;

public class ListaCircularTest extends TestCase
{
	private ListaCircular<String> listaCircular;
	
	//--------------------------------------------
	//Escenarios
	//-------------------------------------------- 
	
	public void setUpEscenario1()
	{
		listaCircular = new ListaCircular<String>();	
	}
	
	public void setUpEscenario2()
	{
		listaCircular=new ListaCircular<String>();
		
		listaCircular.addAtEnd("Juan");
		listaCircular.addAtEnd("Pedro");		
		listaCircular.addAtEnd("el");
	}
	
	//--------------------------------------------
	//Metodos
	//-------------------------------------------- 
	
	public void testListaCircuLar()
	{
		setUpEscenario1();
		assertNull("El atributo debería ser null",listaCircular.darPrimero());
		assertNull("El atributo debería ser null",listaCircular.darUltimo());
		assertEquals( "El atributo siguiente deberia ser 0.",0, listaCircular.getSize() );
	}
	
	public void getSizeTest(){
		setUpEscenario1();
		assertEquals("La lista deberia de estar vacia",0,listaCircular.getSize());
		
		setUpEscenario2();
		assertEquals("La lista deberia tener 3 elementos",3,listaCircular.getSize());
		
	}
	
	public void eliminarTest(){
		setUpEscenario2();
		listaCircular.borrarNodo("Juan");
		assertEquals("Deberia de tener dos nodos",2,listaCircular.getSize());
		
		listaCircular.borrarNodo("elton");
		assertEquals("Deberia de tener dos nodos",2,listaCircular.getSize());
	}
	public void agregarTest(){
		
		setUpEscenario1();
		
		//agrega el primer nodo 
		listaCircular.addAtEnd("Juan");
		assertEquals("Deberia de agregar el nodo",1,listaCircular.getSize());
		
		setUpEscenario2();
		
		listaCircular.addAtEnd("Manuel");
		assertEquals("Deberia de agregar el nodo", 4,listaCircular.getSize());
		
	}
//	public void insertarAntesTest(){
//		setUpEscenario2();
//		assertEquals("No se agrego el nodo",true,listaCircular.insertarAntes("Pedro","Mario"));
//	}
	public void insertarDespuesTest(){
		setUpEscenario2();
	assertEquals("No se agrego el nodo",true,listaCircular.AgregarDespuesDe("Juan", "Sebastian"));
	assertEquals("No se agrego el nodo",true,listaCircular.AgregarDespuesDe("Sebastian", "Camilo"));
	}
	

	
	
	
}
