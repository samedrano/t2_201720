package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop implements Comparable<VOStop>
{
	private int id;
	private String name;
	
	public VOStop(int pId, String pName)
	{
		id = pId;
		name = pName;
	}

	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public int compareTo(VOStop o) {
		// TODO Auto-generated method stub
		return id - o.id;
	}

}
