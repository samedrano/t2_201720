package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.ListaCircular;


public class STSManager implements ISTSManager  {
	//Separador
	public static final String SEPARADOR=",";
	//Listas
	private ListaCircular<Trips> trips;
	private ListaCircular<Stops> stops;
	private DoubleLinkedList<Rutas> rutas;
	private DoubleLinkedList<StopTimes> stopTimes;
	
	@Override
	
	
	public void loadRoutes(String routesFile)throws Exception {
		// Se carga el archivo, lee linea por linea, la primera linea se la salta pues son los encabezados
		 try( FileReader reader = new FileReader( routesFile);
	           BufferedReader in = new BufferedReader( reader );){
			 int contador=0;
	        String linea = in.readLine( );
	        
	      while(linea!=null){
	    	  if(contador==0){
	    		  linea=in.readLine();
	    	  }else{
	    		  contador++;
	    		  String[] partir = linea.split(SEPARADOR);
	    		  //Asigno los atributos
	    		int ruta_ID=Integer.parseInt(partir[0]);
	    		String agency_id=partir[1];
	    		int route_short_name=Integer.parseInt(partir[2]);
	    		String route_long_name=partir[3];
	    		String route_desc=partir[4];
	    		int route_type=Integer.parseInt(partir[5]);
	    		String route_url=partir[6];
	    		String route_color=partir[7];
	    		String route_text_color=partir[8];
	    		  //Creo la nueva info del nodo y lo agrego
	    		  Rutas este= new Rutas(ruta_ID,agency_id,route_short_name,route_long_name,route_desc,route_type,route_url,route_color,route_text_color);
	    		 rutas.addAtEnd(este); 	
	    		  linea=in.readLine();
	    	  }
	    	  
	      }
	        
		 }catch (Exception e){
			 throw new Exception ("No se pudo cargar el archivo");
		 }
		
	}

	@Override
	public void loadTrips(String tripsFile)throws Exception {
		// Se carga el archivo, lee linea por linea, la primera linea se la salta pues son los encabezados 
		 try( FileReader reader = new FileReader( tripsFile);
	           BufferedReader in = new BufferedReader( reader );){
			 
			 int contador=0;
	        String linea = in.readLine( );
	       
	       
	      while(linea!=null){
	    	  if(contador==0){
	    		  linea=in.readLine();
	    	  }else{
	    		  contador++;
	    		  String[] partir = linea.split(SEPARADOR);
	    		  //Asigno los atributos
	    		     int routeID=Integer.parseInt(partir[0]);
	    			 int serviceID=Integer.parseInt(partir[1]);;
	    			 int tripID=Integer.parseInt(partir[2]);
	    			 String tripHeadSign=partir[3];
	    			 String tripShortName=partir[4];
	    			 int directionID=Integer.parseInt(partir[5]);
	    			 int blockID=Integer.parseInt(partir[6]);
	    			 int shapeID=Integer.parseInt(partir[7]);
	    			 int wheelchairAccesible=Integer.parseInt(partir[8]);
	    			 int bikesAllowed=Integer.parseInt(partir[9]);
	    		  
	    		  //Creo la nueva info del nodo y lo agrego
	    			 Trips nuevo = new Trips(routeID,serviceID,tripID,tripHeadSign,tripShortName,directionID,blockID,shapeID,wheelchairAccesible,bikesAllowed);	
	    			 
	    		 trips.AgregarEnOrdenAscendente(nuevo); 	
	    		  linea=in.readLine();
	    	  }
	    	  
	      }
	        
		 }catch (Exception e){
			 throw new Exception ("No se pudo cargar el archivo");
		 }
		// TODO Auto-generated method stub

	}

	@Override
	public void loadStopTimes(String stopTimesFile) throws Exception {
		// Se carga el archivo, lee linea por linea, la primera linea se la salta pues son los encabezados
		 try( FileReader reader = new FileReader( stopTimesFile);
	           BufferedReader in = new BufferedReader( reader );){
			 int contador=0;
	        String linea = in.readLine( );
	   
	      while(linea!=null){
	    	  if(contador==0){
	    		  linea=in.readLine();
	    	  }else{
	    		  contador++;
	    		  String[] partir = linea.split(SEPARADOR);
	    		// Asigno los atributos  
	    		  int trip_id=Integer.parseInt(partir[0]);
	    		  String arrival_time=partir[1];
	    		  String departure_time=partir[2];
	    		  int stopID=Integer.parseInt(partir[3]);;
	    		  int stopSequence=Integer.parseInt(partir[4]);;
	    		  String stopHeadSign=partir[5];
	    		  int pickupType=Integer.parseInt(partir[6]);;
	    		  int dropOfftype=Integer.parseInt(partir[7]);;
	    		  double shapeDistTravel=Double.parseDouble(partir[8]);
	    		  
	    		  // Creo la nueva info del nodo y lo agrego
	    		 StopTimes nuevo= new StopTimes(trip_id, arrival_time, departure_time, stopID, stopSequence, stopHeadSign, pickupType, dropOfftype, shapeDistTravel);
	    		 stopTimes.addAtEnd(nuevo); 	
	    		 
	    		  linea=in.readLine();
	    	  }
	    	  
	      }
	        
		 }catch (Exception e){
			 throw new Exception ("No se pudo cargar el archivo");
		 }
		
		
	}

	@Override
	public void loadStops(String stopsFile)throws Exception {
		// Se carga el archivo, lee linea por linea, la primera linea se la salta pues son los encabezados
		 try( FileReader reader = new FileReader( stopsFile);
	           BufferedReader in = new BufferedReader( reader );){
			 int contador=0;
	        String linea = in.readLine( );
	        
	       
	        
	        
	      while(linea!=null){
	    	  if(contador==0){
	    		  linea=in.readLine();
	    	  }else{
	    		  contador++;
	    		  String[] partir = linea.split(SEPARADOR);
	    		// Asigno los atributos  
	    		  
	    		   int stopID=Integer.parseInt(partir[0]);
	    			 int stopCode=Integer.parseInt(partir[1]);
	    			 String stopName=partir[2];
	    			 String stopDesc=partir[3];
	    			 double stopLat=Double.parseDouble(partir[4]);
	    			 double stopLon=Double.parseDouble(partir[5]);
	    			 String zoneID=partir[6];
	    			 String stopURL=partir[7];
	    			 int locationType=Integer.parseInt(partir[8]);
	    			 String parentStation=partir[9];
	    		  
	    		  // Creo la nueva info del nodo y lo agrego
	    		 Stops nuevo= new Stops(stopID,stopCode,stopName,stopDesc,stopLat,stopLon,zoneID,stopURL,locationType,parentStation);
	    		  	
	    		 stops.AgregarEnOrdenAscendente(nuevo);
	    		  linea=in.readLine();
	    	  }
	    	  
	      }
	        
		 }catch (Exception e){
			 throw new Exception ("No se pudo cargar el archivo");
		 }
		// TODO Auto-generated method stub
	
		
		
	}
	public Stops buscarParadaPorNombre( String name)
	{
		int contador =0;
		boolean encontro = false;
		Stops parada = null;
		// busca el stop dado por parametro
		while(contador < stops.getSize()||!encontro)
		{
			parada = stops.getElement();
			if(parada.darStopName().equals(name))
			{
				encontro= true;
			}
			stops.next();
			contador++;
		}
		return parada;
	}

	/**
	 * Method to calculate all the routes that stop at a given stop
	 * @param stopName - name of the stop to search routes for
	 * @return List of route objects making a stop at the given stop
	 */
	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		IList<VORoute> lista= new ListaCircular<VORoute>();
		
		Stops parada = buscarParadaPorNombre(stopName);
		int paradaId = parada.darStopId();
		// busca el stopTime que tiene el stopId del stop dado por parametro
		int contador=0;
		boolean encontro= false;
		StopTimes sTime= null;
		while(contador < stopTimes.getSize() || !encontro)
		{
			sTime = stopTimes.getElement();
			if(sTime.darStopID()== paradaId){
				encontro =true;
			}
			stopTimes.next();
			contador++;	
		}
		int stopTimeTripId = sTime.darTripID();
		
		int contador2=0;
		Trips trip= null;
		while(contador2 < trips.getSize())
		{
			trip = trips.getElement();
			if(trip.darTripID()== stopTimeTripId){
				int rutaId = trip.darRouteID();
				
				int contador3=0;
				Rutas rut = null;
				while( contador3 < rutas.getSize()){
					rut = rutas.getElement();
					if (rut.darRutaID() == rutaId)
					{
						VORoute i = new VORoute(rut.darRutaID(), rut.darRouteLongName());
						lista.addAtEnd(i);
					}
					rutas.next();
					contador++;
				}
			}
			trips.next();
			contador++;	
		}
		return lista;
	}
	
	/**
	 * Method to obtain all stops in a route. This stops must be ordered in accordance 
	 * to the route direction (distance from the closest to the furthers from the initial stop)
	 * @param routeName - name of the route to search for
	 * @param direction - directions in which we want to find the stops
	 * @return Ordered list of stops in the route for the given direction.
	 */
	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		IList<VOStop> listaDeParadas = new ListaCircular<VOStop>();
		int contador = 0;
		boolean encontro = false;
		//Recorre los trips
		while (contador < trips.getSize() && encontro != true) {
			//Saca la direccion del trip actual y la routeID del trip actual
			String direccion = trips.getElement().darTripHeadSign();
			int tripRouteID=trips.getElement().darRouteID();
			//si la direccíon del trip es igual a la pasada por parametro este entra
			if(direccion.equals(direction)){

			int contador2 = 0;
			//Recorre las rutas
			while (contador2 < rutas.getSize()&& encontro!=true) {
				//saca las el nombre de la ruta y el id.
				String nombreRuta = rutas.getElement().darRouteLongName();
				int routaID=rutas.getElement().darRutaID();
				// Si el nombre de la ruta y el ID de esta es el mismo que el ID del trip en la dirección escogida previamente entra a recorrer los stopTimes
				if (nombreRuta.equals(routeName) && tripRouteID==routaID ){
					encontro=true;
					//busca el tripID del trip actual
					int IDtrip = trips.getElement().darTripID();

					int contador3 = 0;
					//Recorre los StopTimes
					while (contador3 < stopTimes.getSize()) {
						int parada = stopTimes.getElement().darTripID();
						//Si el tripID de la parada es igual al tripID del trip entonces entra al if
						if (IDtrip == parada) {
							boolean yaEncontro = false;
							//Guardo el stopID de la parada actual
							int stopID = stopTimes.getElement().darStopID();
							int contador4 = 0;
							// Entra a buscar el nombre de la parada 
							while (yaEncontro == false || contador4 < stops.getSize()) {
								int stopIDaComparar = stops.getElement().darStopId();
								// si el Id son iguales entonces se agrega a la lista de paradas en la ruta.
								if (stopID == stopIDaComparar) {
									String nombreStop = stops.getElement().darStopName();

									VOStop paradaAgregar = new VOStop(stopID, nombreStop);
									listaDeParadas.add(paradaAgregar);
									yaEncontro = true;
								}
								contador4++;
								stops.next();
							}

						}
						contador3++;
						stopTimes.next();
					}

				}
				contador2++;
				rutas.next();
			}
			
			}

			contador++;
			trips.next();
		}

		return listaDeParadas;
	}

}
