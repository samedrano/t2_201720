package model.data_structures;

import java.util.Iterator;

public class ListaCircular<T extends Comparable<T>> implements IList<T>
{
	private Nodo<T> prim;
	private Nodo<T> ult;
	private int size;
	
	private Nodo<T> actual;
	private int posicionAct;
	
	//--------------------------------------------
	//Constructor
	//-------------------------------------------- 
	
	public ListaCircular()
	{
		prim = null;
		ult= null;
		size=0;
		actual = prim;
		posicionAct = 0;
	}
	
	//--------------------------------------------
	//Metodos
	//-------------------------------------------- 
	
	public Nodo<T> darPrimero()
	{
		return prim;
	}
	public Nodo<T> darUltimo()
	{
		return ult;
	}	
	
	public void addAtEnd( T info)
	{
		Nodo<T> nuevo = new Nodo<T>( info);
		if ( prim == null)
		{			
			actual=nuevo;
			prim = nuevo;
			ult = nuevo;
			size ++;
		}
		else 
		{	
			ult.cambiarSiguiente(nuevo);
			nuevo.cambiarSiguiente(prim);
			prim.cambiarAnterior(nuevo);
			nuevo.cambiarAnterior(ult);
			
			ult=nuevo;
			actual=ult;
			size ++;
		}
	}
	
	public int getSize(){
		return size;
	}

//	public boolean insertarAntes(T nodoReferencia, T nodoInsertar)
//	{
//		Nodo<T> buscado= buscarNodo(nodoReferencia);
//		
//		if(buscado==null){
//			return false;
//		}else{
//			
//			Nodo<T> insert= (Nodo<T>)nodoInsertar;
//			Nodo<T> ref= (Nodo<T>)nodoReferencia;
//			(insert).cambiarSiguiente(ref);
//			(ref).darAnterior().cambiarSiguiente(insert);
//			
//			return true;
//		}
//		
//	}
	public boolean AgregarDespuesDe(T nodoReferencia, T nodoInsertar)
	{
		Nodo<T> ref= buscarNodo(nodoReferencia);
		Nodo<T> insert= new Nodo<T>(nodoInsertar);
		
		if(ref==null){
			return false;
		}else{
			insert.cambiarSiguiente(ref.darSiguiente());
			insert.cambiarAnterior(ref);
			ref.darSiguiente().cambiarAnterior(insert);
			ref.cambiarSiguiente(insert);	
			actual=insert;
			size++;
			return true;
		}
		
	}
	
	
	
	public boolean AgregarEnOrdenAscendente(T nodoInsertar){
		Nodo<T> insert = new Nodo<T>(nodoInsertar);
		Nodo<T> actual = prim;
		boolean ya = false;
		if(prim==null){
			prim=insert;
			ult=insert;
			size++;
			ya=true;
		}else
		{
			while(actual!=null || ya==false){
				
				if(actual.darInformacion().compareTo(insert.darInformacion())<=0 & actual.darSiguiente().darInformacion().compareTo(insert.darInformacion())>=0){
					insert.cambiarSiguiente(actual.darSiguiente());
					actual.cambiarSiguiente(insert);
					insert.cambiarAnterior(actual);
					actual.darSiguiente().cambiarAnterior(insert);
					ya = true;
				}
				
			}
		}
		return ya;
		
	}
	
	
	
	public boolean borrarNodo(T este)
	{
		Nodo<T> buscado = buscarNodo(este);
		if(buscado==null){
			return false;
		}else{
			Nodo<T> sig = buscado.darSiguiente();
			Nodo<T> ant = buscado.darAnterior();
		
			buscado.darAnterior().cambiarSiguiente(sig);
			buscado.darSiguiente().cambiarAnterior(ant);
			
			size--;
			return true;
		}
		
	}
	public boolean esVacia()
	{
		if (prim == null)
		{
			return true;
		}
		return false;
	}
	
	//Diferencia entre T y Nodo<T>
	
	public Nodo<T> buscarNodo(T info)
	{
		Nodo<T> actual=prim;
		int contador =0;
		boolean encontro = false;
		
		while(contador<size || encontro==false)
		{
			 contador++;
			 
			if(actual.darInformacion().compareTo(info)==0){
				encontro=true;
				return actual;
			}
		
		actual=actual.darSiguiente();	
		}
		//si lo encuentra retorna el actual. Si no, llega al return null
		return null;
	}
	
//	public Nodo<T> buscarNodoPorPosicion(T info,int pos)
//	{
//		Nodo<T> actual=prim;
//		int contador =0;
//		boolean encontro = false;
//
//		while(contador<size || encontro==false)
//		{
//			if(actual.darInformacion().compareTo(info)==0 && contador==pos){
//				encontro = true;
//				return actual;
//			}		
//			contador++;
//			actual=actual.darSiguiente();	
//		}
//		//si lo encuentra retorna el actual. Si no, llega al return null
//		return null;
//	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(T elemento) {
		Nodo<T> nuevo = new Nodo<T>(elemento);
		Nodo<T> ant= actual.darAnterior();
		
		ant.cambiarSiguiente(nuevo);
		nuevo.cambiarSiguiente(actual);
		actual.cambiarAnterior(nuevo);
		nuevo.cambiarAnterior(ant);
		actual = nuevo;
		size++;
		//Se agrega antes del actual para agregar en la posicion actual.
		
	}
	public boolean delete() {
		Nodo<T> sig= actual.darSiguiente();
		Nodo<T> ant= actual.darAnterior();

		ant.cambiarSiguiente(sig);
		sig.cambiarAnterior(ant);
		actual= sig;
		size--;
		return true;
		// el nuevo actual sera el siguiente para mantener la misma posicion.

	}
	@Override
	public T getElement() {
		return actual.darInformacion();
	}

	@Override
	public void addAtk(T elemento, int pos) {
		if( pos<0 || pos>=size){
			//no se hace nada
		}else{
			Nodo<T> nuevo = new Nodo<T>(elemento);
			Nodo<T> refer = prim;
			int contador = 0;
			while( contador<size || contador != pos )
			{
				refer = refer.darSiguiente();
				contador++;
			}
			Nodo<T> ant = refer.darAnterior();
			ant.cambiarSiguiente(nuevo);
			nuevo.cambiarSiguiente(refer);
			refer.cambiarAnterior(nuevo);
			nuevo.cambiarAnterior(ant);
			// se incrementa la posicion porque se agrego un nodo antes del actual
			if( pos <= posicionAct){
				posicionAct++;
				//se actualizan los valores del ultimo y el primero si es el caso
			}if(pos == size-1){
				ult = nuevo;
			}else if(pos == 0){
				prim = nuevo;}

			size++;
		}
	}

	@Override
	public void deleteAtK(int pos) {
		if( pos<0 || pos>=size){
			//no se hace nada
		}else{
			int contador = 0;
			Nodo<T> refer = prim;
			while( contador<size || contador!=pos)
			{
				refer= refer.darSiguiente();
				contador++;
			}
			Nodo<T> sig = refer.darAnterior();
			Nodo<T> ant = refer.darSiguiente();
			sig.cambiarAnterior(ant);
			ant.cambiarSiguiente(sig);
			// si el nodo a quitar es el actual, el nuevo actual sera sig para no cambiar el valor de posicion
			if( refer == actual){
				actual= sig;
			}
			// se disminuye la posicion porque se agrego un nodo antes del actual
			if( pos < posicionAct){
				posicionAct--;
			}
			//se actualizan los valores del ultimo y el primero si es el caso
			if(pos == size-1){
				ult = ant;
			}else if(pos == 0){
				prim = sig;}
			size--;
		}
	}
	public T getElementAtK(int pos)
	{
		if( pos<0 || pos>=size){
			//no se hace nada
			return null;
		}
		int contador = 0;
		Nodo<T> refer = prim;
		while( contador<size || contador!=pos)
		{
			refer= refer.darSiguiente();
			contador++;
		}
		return refer.darInformacion();
	}

	@Override
	public void next() {
		actual = actual.darSiguiente();
		posicionAct++;
	}

	@Override
	public void previous() {
		actual = actual.darAnterior();
		posicionAct++;
	}
	
}
