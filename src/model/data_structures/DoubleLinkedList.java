package model.data_structures;

import java.util.Iterator;

public class DoubleLinkedList<T extends Comparable<T>> implements IList<T>
{	
	private Nodo<T> prim;
	private Nodo<T> ult;
	private int size;
	private Nodo<T> actual;
	private int posicionAct;
	
	//--------------------------------------------
	//Constructor
	//-------------------------------------------- 
	
	public DoubleLinkedList()
	{
		prim = null;
		ult= null;
		size=0;
		actual = prim;
		posicionAct = 0;
	}
	
	//--------------------------------------------
	//Metodos
	//-------------------------------------------- 

	public Nodo<T> darPrimero()
	{
		return prim;
	}
	public Nodo<T> darUltimo()
	{
		return ult;
	}	
	public void addAtEnd(T info){
		
		Nodo<T> nuevo = new Nodo<T>(info);
		
		if(prim==null)
		{
			prim=nuevo;
			ult=nuevo;		
			size ++;
			
		}else{
			ult.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ult);
			ult=nuevo;
			size ++;	
		}	
	}
	
	public boolean agregarDespuesDe(T elemReferencia, T elemInsertar){
		Nodo<T> ref = buscarNodo(elemReferencia);
		Nodo<T> insert= new Nodo<T>(elemInsertar);
		if(ref==null)
		{
			return false;
		}
		else if(ref.darInformacion().compareTo((T) ult.darInformacion())==0){
			ult.cambiarSiguiente(insert);
			insert.cambiarAnterior(ult);
			ult=insert;
			size ++;
			return true;
		}
		else if(ref.darInformacion().compareTo((T) prim.darInformacion())==0){			
			insert.cambiarSiguiente(ref);
			ref.cambiarAnterior(insert);
			prim=insert;
			size++;
			return true;					
		}else{
			insert.cambiarSiguiente(ref.darSiguiente());
			insert.cambiarAnterior(ref);
			ref.darSiguiente().cambiarAnterior(insert);
			ref.cambiarSiguiente(insert);			
			size++;
			return true;
		}
		
	}
	
	public int getSize()
	{
		return size;
	}
	
	public boolean esVacia()
	{
		if (prim == null)
		{
			return true;
		}
		return false;
	}
	
	public boolean borrarNodo(T este)
	{
		Nodo<T>buscado = buscarNodo(este);
		Nodo<T> sig = buscado.darSiguiente();
		Nodo<T> ant = buscado.darAnterior();
		if(buscado==null)
		{
			return false;			
		}else if(buscado.darInformacion().compareTo(prim.darInformacion())==0){
			prim.cambiarSiguiente(sig);
			sig.cambiarAnterior(null);
			prim=sig;
			size--;
			return true;
		}else if(buscado.darInformacion().compareTo(ult.darInformacion())==0){	
			ant.cambiarSiguiente(null);
			buscado.cambiarAnterior(null);
			ult=ant;
			size--;
			return true;
			
		}else{
			ant.cambiarSiguiente(sig);
			sig.cambiarAnterior(ant);
			size--;
			return true;
		}
	}
	
	public Nodo<T> buscarNodo(T info)
	{
		Nodo<T> actual=prim;
		boolean encontro = false;
		
		while(actual!=null || encontro==false)
		{
			if(actual.darInformacion().compareTo( info)==0)
			{
				encontro=true;
				return actual;
			}
		actual=actual.darSiguiente();			
		}
		//si lo encuentra retorna el actual. Si no, llega al return null
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void add(T elemento) {
		Nodo<T> nuevo = new Nodo<T>(elemento);
		Nodo<T> ant= actual.darAnterior();
		if (actual == null){
			
		}else if( actual== prim){
			nuevo.cambiarSiguiente(actual);
			actual.cambiarAnterior(nuevo);
			actual = nuevo;
			size++;
		}else{
		ant.cambiarSiguiente(nuevo);
		nuevo.cambiarSiguiente(actual);
		actual.cambiarAnterior(nuevo);
		nuevo.cambiarAnterior(ant);
		actual = nuevo;
		size++;
		//Se agrega antes del actual para agregar en la posicion actual.
		}
	}
	public boolean delete() {
		Nodo<T> sig= actual.darSiguiente();
		Nodo<T> ant= actual.darAnterior();
		if(actual == null){
			return false;
		}
		else{
			ant.cambiarSiguiente(sig);
			sig.cambiarAnterior(ant);
			actual= sig;
			size--;
			return true;
		}
		// el nuevo actual sera el siguiente para mantener la misma posicion.
	}

	@Override
	public T getElement() {
		if(actual == null){
			return null;
		}
		else
		return actual.darInformacion();
	}

	@Override
	public void addAtk(T elemento, int pos) {
		Nodo<T> nuevo = new Nodo<T>(elemento);
		Nodo<T> refer = prim;
		if( pos<0 || pos>=size){
			//no se hace nada
		}else{
			//si el nodo a agregar sera el primero
			if(pos==0){
				nuevo.cambiarSiguiente(prim);
				prim.cambiarAnterior(nuevo);
				prim = nuevo;
				size++;
			}
			//si el nodo a agregar sera el ultimo
			else if(pos == size-1){
				ult.cambiarSiguiente(nuevo);
				nuevo.cambiarAnterior(ult);
				ult= nuevo;		
				size++;
			}
			else{
				int contador = 0;
				while( contador<size || contador != pos )
				{
					refer = refer.darSiguiente();
					contador++;
				}
				Nodo<T> ant = refer.darAnterior();
				ant.cambiarSiguiente(nuevo);
				nuevo.cambiarSiguiente(refer);
				refer.cambiarAnterior(nuevo);
				nuevo.cambiarAnterior(ant);
				size++;	
				// se incrementa la posicion porque se agrego un nodo antes del actual
				if( pos <= posicionAct){
					posicionAct++;}
			}
		}
	}

	@Override
	public void deleteAtK(int pos) {
		int contador = 0;
		Nodo<T> refer = prim;
		if( pos<0 || pos>=size){
			//no se hace nada
		}else{
			//si el nodo a eliminar es el primero
			if( pos==0){
				Nodo<T> sig = refer.darAnterior();
				sig.cambiarAnterior(null);
				prim= sig;
			//si el nodo a eliminar es el ultimo
			}else if(pos==size-1){
				Nodo<T> ant = refer.darSiguiente();
				ant.cambiarSiguiente(null);
				ult = ant;
			}else{
				while( contador<size || contador!=pos)
				{
					refer= refer.darSiguiente();
					contador++;
				}
				Nodo<T> sig = refer.darAnterior();
				Nodo<T> ant = refer.darSiguiente();
				sig.cambiarAnterior(ant);
				ant.cambiarSiguiente(sig);
				// si el nodo a quitar es el actual, el nuevo actual sera sig para no cambiar el valor de posicion
				if( refer == actual){
					actual= refer.darSiguiente();
				}
			}
			// se disminuye la posicion porque se agrego un nodo antes del actual
			if (pos < posicionAct){
				posicionAct--;
			}
		}
	}
	public T getElementAtK(int pos)
	{
		if( pos<0 || pos>=size){
			//no se hace nada
			return null;
		}
		int contador = 0;
		Nodo<T> refer = prim;
		while( contador<size || contador!=pos)
		{
			refer= refer.darSiguiente();
			contador++;
		}
		return refer.darInformacion();
	}

	@Override
	public void next() {
		if(actual == null){
			
		}
		else
		actual = actual.darSiguiente();
		posicionAct++;
	}

	@Override
	public void previous() {
        if(actual == null){
			
		}
		actual = actual.darAnterior();
		posicionAct--;
	}	
}
