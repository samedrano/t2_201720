package model.logic;

public class Trips implements Comparable <Trips>{

	private int routeID;
	private int serviceID;
	private int tripID;
	private String tripHeadSign;
	private String tripShortName;
	private int directionID;
	private int blockID;
	private int shapeID;
	private int wheelchairAccesible;
	private int bikesAllowed;
	
	public Trips(int pRutaID, int pServiceID,int ptripID, String pTripheadSign, String pTripShortName,int pDirectionID, int pBlockID, int pShapeID, int pWheelA,int pBikesA ){
		routeID=pRutaID;
		serviceID=pServiceID;
		tripID=ptripID;
		tripHeadSign=pTripheadSign;
		tripShortName=pTripShortName;
		directionID=pDirectionID;
		blockID=pBlockID;
		shapeID=pShapeID;
		wheelchairAccesible=pWheelA;
		bikesAllowed=pBikesA;
		
	}
	
	public int darRouteID(){
		return routeID;
	}
	public int darServiceID(){
		return serviceID;
	}
	
	public int darTripID(){
		return tripID;
	}
	public String darTripHeadSign(){
		return tripHeadSign;
	}
	public String darTripShortName(){
		return tripShortName;
	}
	public int darDirectionID(){
		return directionID;
	}
	public int darBlockID(){
		return blockID;
	}
	public int darShapeID(){
		return shapeID;
	}
	public int darWheelChairAccessible(){
		return wheelchairAccesible;
	}
	public int darBikesAllowd(){
		return bikesAllowed;
	}
	@Override
	public int compareTo(Trips o) {
		
		
			return tripID-o.darTripID();
		
		// TODO Auto-generated method stub
		
	}

}
