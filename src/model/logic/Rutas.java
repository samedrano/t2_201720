package model.logic;

import com.sun.org.apache.bcel.internal.generic.NEW;

public class Rutas implements Comparable<Rutas> {
	
	private int ruta_ID;
	private String agency_id;
	private int route_short_name;
	private String route_long_name;
	private String route_desc;
	private int route_type;
	private String route_url;
	private String route_color;
	private String route_text_color;
	
public Rutas(int pID, String pAgency_ID, int pRouteShortName,String plongName,String pRoute_desc,int pTipo, String pUrl,String pColor,String ptextoColor){
	
	ruta_ID=pID;
	agency_id=pAgency_ID;
	route_short_name=pRouteShortName;
	route_long_name=plongName;
	route_desc=pRoute_desc;
	route_type=pTipo;
	route_url=pUrl;
	route_color=pColor;
	route_text_color=ptextoColor;

}

public int darRutaID(){
	return ruta_ID;
}

public String darAgencyID(){
	return agency_id;
}
public int darRouteShortName(){
	return route_short_name;
}
public String darRouteLongName(){
	return route_long_name;
}
public String darRouteDes(){
	return route_desc;
}
public int darRouteType(){
	return route_type;
}
public String darRouteUrl(){
	return route_url;
}
public String darRouteColor(){
	return route_color;
}
public String darRouteTextColor(){
	return route_text_color;
}

public int compareTo(Rutas pRuta) {
	// TODO Auto-generated method stub
		
	if (pRuta.darRutaID()==ruta_ID){
		return 0;	 	
	}else{
		return ruta_ID-pRuta.darRutaID(); 
	}

}


}
