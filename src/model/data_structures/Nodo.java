package model.data_structures;

public class Nodo<T extends Comparable<T>>
{
	private T info;
	private Nodo<T> sig;
	private Nodo<T> ant;
	
	public Nodo( T pInfo)
	{
		info = pInfo;
		sig = null;
		ant = null;			
	}
	public void cambiarSiguiente( Nodo<T> pSig)
	{
		sig = pSig;
	}
	public void cambiarAnterior( Nodo<T> pAnt)
	{
		ant = pAnt;
	}
	public T darInformacion()
	{
		return info;
	}
	public Nodo<T> darSiguiente()
	{
		return sig;
	}
	public Nodo<T> darAnterior()
	{
		return ant;
	}
	

}
