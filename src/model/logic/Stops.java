package model.logic;

public class Stops implements Comparable<Stops> {

	private int stopID;
	private int stopCode;
	private String stopName;
	private String stopDesc;
	private double stopLat;
	private double stopLon;
	private String zoneID;
	private String stopURL;
	private int locationType;
	private String parentStation;
	
	public Stops(int pStopID, int pStopCode, String pStopName, String pStopDesc, double pstopLat, double pStopLon, String pZoneID,String pZoneUrl, int plocationType, String pParentStation){
		stopID=pStopID;
		stopCode=pStopCode;
		stopName=pStopName;
		stopDesc=pStopDesc;
		stopLat=pstopLat;
		stopLon=pStopLon;
		zoneID=pZoneID;
		stopURL=pZoneUrl;
		locationType=plocationType;
		parentStation=pParentStation;	
	}
	public int darStopId(){
		return stopID;
	}
	
	public int darStopCode(){
		return stopCode;
	}
	public String darStopName(){
		return stopName;
	}
	public String darStopDesc(){
		return stopDesc;
	}
	public double darStopLat(){
		return stopLat;
	}
	public double darStopLon(){
		return stopLon;
	}
	public String darZoneID(){
		return zoneID;
	}
	public String darStopUrl(){
		return stopURL;
	}
	public int darLocationType(){
		return locationType;
	}
	public String darParentStation(){
		return parentStation;
	}
	
	@Override
	// Lo comparo por el stopID
	public int compareTo(Stops o) {
		// TODO Auto-generated method stub
		
		return stopID-o.stopID;
	}

}
